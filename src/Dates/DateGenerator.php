<?php namespace Mikko\Dates;

use Carbon\Carbon;

class DateGenerator {
	private $theSalaryStartingDate;
	private $theBonusStartingDate;

    public function __construct()
    {
        Carbon::mixin(new MikkoDateMixin());
	}

	public function getDates(String $year){
		// return $testDates = [
	 //        [ 'February', 28, 15 ],
	 //        [ 'March', 29, 15 ],
	 //        [ 'April', 30, 15 ],
	 //        [ 'May', 31, 15 ],
	 //        [ 'June', 28, 19 ],
	 //        [ 'July', 31, 15 ],
	 //        [ 'August', 30, 15 ],
	 //        [ 'September', 30, 18 ],
	 //        [ 'October', 31, 15 ],
	 //        [ 'November', 29, 15 ],
	 //        [ 'December', 31, 18 ],
  //   	];
		return $this->generateDates($year);
	}

	private function generateDates(String $year){
		$startingDateSalary = $this->getStartingDates($year);
		$dates = [];
		
		$dates = $this->loopToGenerateDates('Salary', $year, $dates);
		$dates = $this->loopToGenerateDates('Bonus', $year, $dates);

		return $dates;
	}

	private function loopToGenerateDates(String $kind, String $year, Array $dates){
		$method = "next{$kind}Day"; 
		$dateName = "the{$kind}StartingDate";
		$endDate = Carbon::parse('first day of December' . $year);
		$date = $this->$dateName->clone();
		while($endDate->greaterThan($date)){
			$date->$method();
			$dates = $this->addToArray($date, $dates);
		}

		return $dates;
	}

	private function addToArray(Carbon $date, Array $dates){
		$month = $date->month;
		if(empty($dates[$month])){
			$dates[$month] = [];
			$dates[$month][] = $date->englishMonth;
			$dates[$month][] = $date->day;
		} else {
			$dates[$month][] = $date->day;
		}

		return $dates;
	}

	private function getStartingDates(String $year){
		$start = Carbon::now();
		if($start->year != (int)$year){
			$start = Carbon::parse('first day of January ' . (string)$year);
		}

		$this->theBonusStartingDate = $start->clone();

		if(! $start->day < 16){
			$this->theBonusStartingDate->modify('first day of previous month');
		}
		
		$this->theSalaryStartingDate = $start->clone()->subMonth();
	}
}