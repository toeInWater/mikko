<?php namespace Mikko\Dates;

Class MikkoDateMixin{
	public function nextSalaryDay()
	{
		return function(){
			$this->modify('last day of next month');
             if(! $this->isWeekday($this))
			 {
			 	$this->modify('previous weekday');
			 }
			
			return $this;
		};
	}

	public function nextBonusDay()
	{
		return function(){
			$this->modify('last day of this month')->addDays(15);
			if(! $this->isWeekday())
			{
				$this->modify('next wednesday');
			} 
			
			return $this;
		};
	}	

	private function isWeekday(){
		return $this->dayOfWeekIso < 6;
	}
}
