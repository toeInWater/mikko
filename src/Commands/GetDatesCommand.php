<?php namespace Mikko\Commands;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class getDatesCommand extends Command {
    private $dateGenerator;

    public function __construct(\Mikko\Dates\DateGenerator $dateGenerator)
    {
        $this->dateGenerator = $dateGenerator;
        parent::__construct();
    }

    /**
     * Configure the command.
     */
    public function configure()
    {
        $this->setName('getDates')
             ->setDescription('Output a file containing all payment dates for a year.  The default is a csv file for the current year containing both salary payment dates and bonus payment dates.')
             ->addArgument('year', 
                InputArgument::REQUIRED, 
                'The year for which you want to get the dates.')
             ->addOption(
                'file', 
                'f', 
                InputOption::VALUE_OPTIONAL, 
                'The filename and the path to where you want to store it.  Eg. "C:\myDatesFolder
                \whenToPay.csv".  The default when not provided is "dates.csv".', 
                'dates.csv'
            );
    }

    /**
     * Execute the command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $year = (int)$input->getArgument('year');
        $dates = $this->dateGenerator->getDates($year); 
        $this->generateFile($dates, $input->getOption('file'));
        $this->showDates($output, $dates);
    }
}