<?php namespace Mikko\Commands;

use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;
use League\Csv\Writer;

class Command extends SymfonyCommand {
    private $headers = ['Year', 'Salary Date', 'Bonus Date'];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show a table of all dates on the cli.
     * @param OutputInterface $output
     * @param Array $dates
     */
    protected function showDates(OutputInterface $output, Array $dates)
    {
        $output->writeln('<info>Your file is being generated.  The dates below will be written to it.</info>');
        $table = new Table($output);
        $table->setHeaders($this->headers)
              ->setRows($dates)
              ->render();
    }

    protected function generateFile(Array $dates, String $path){    
        try {
            $csv = Writer::createFromFileObject(new \SplFileObject($path, 'w'));
            $csv->insertOne($this->headers);
            $csv->insertAll($dates);
        } catch (Exception $e) {
            echo $e->getMessage(), PHP_EOL;
        }
    }
}