# Project voor toegangsproef iBuidings

## Status:
Dit project is onder ontwikkeling.

## Gebruik:
- *Optie 1*: Geef het commando `php mikko <commando>` in op de command line in de folder waar deze code geïnstalleerd werd.  Vervang "<commando>" door het één van de ondersteunde commando's.
- *Optie 2*: Indien de phar file gebruikt wordt dan is het commando `php mikko.phar <commando>`.  Vervang "<commando>" door het één van de ondersteunde commando's.
- *Optie3*: Gebruik de Dockerfile om de phar in een docker container te gebruiken.

## Ondersteunde commando's:
Op heden ondersteunt het programma volgende commando's:
- `getDates <jaar>`: vervang "<jaar>" door het jaar waarvoor u de data wil krijgen.  Dit zal de gevraagde data weergeven zowel op de command line (tabelvorm) als in een csv bestand.
	- Opties:
		- `--file` of `-f`: een filenaam (al dan niet met absoluut / relatief pad) dat moet gebruikt worden als naam / pad voor het te genereren csv bestand.  De default is "dates.csv"

## Gebruikte libraries:
- [Symfony Config](https://symfony.com/components/Config) voor het eenvoudig werken met configuratie-instellingen (in opbouw)
- [Symfony Console](https://symfony.com/components/Console) voor de CLI-toepassing
- [CSV](https://csv.thephpleague.com/) voor het aanmaken van het CSV-bestand
- [Carbon](https://github.com/briannesbitt/carbon) voor het eenvoudig manipuleren van data
- [Box](https://github.com/humbug/box/blob/master/doc/installation.md#composer) werd gebruikt voor de creatie van het phar-bestand en de dockerfile
