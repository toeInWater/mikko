#TODO:

## lijst van commando's / opties
- get dates
	- optie enkel salary
	- optie enkel bonus
	- optie filepath
	- TODO: default! optie om jaar mee te geven
- get salary dates
- get bonus dates
=> symfony/console (https://symfony.com/components/Console)

## DONE: integratie datum-functionaliteit
- jaar bepalen
- bonusdag bepalen / maand
- salarisdag bepalen / maand
=> carbon (https://github.com/briannesbitt/Carbon) via mixin

## DONE: integratie CSV
- kolom voor naam van de maand
- kolom voor salarisdatum
- kolom voor bonusdatum
=> CSV (https://csv.thephpleague.com/)

## want to haves
- integratie phpstan (https://github.com/phpstan/phpstan)
- integratie unit tests (phpunit)
- basic documentatie

## nice to haves
- logging
- statistieken?
- conversie naar andere formaten (bv. HTML/json)
- feestdagen?
- configuratie (https://symfony.com/components/Config)
- klantvriendelijke documentatie